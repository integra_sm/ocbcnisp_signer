<!DOCTYPE html>
<html>
<!-- {{ assets(); }} -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Create Digital signatures and Sign PDF documents online.">
    <meta name="author" content="OCBC NISP">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo assets('uploads/app/'.env("APP_ICON")); ?>">
    <title>Upload Wizard | OCBC NISP</title>


    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.4.2/css/buttons.dataTables.min.css" />

    <!-- Ion icons -->
    <link href="{{ assets('assets/fonts/ionicons/css/ionicons.css') }}" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link href="{{ assets('assets/libs/bootstrap/css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ assets('assets/css/simcify.min.css') }}" rel="stylesheet">
    <!-- Signer CSS -->
    <link href="{{ assets('assets/css/style.css') }}" rel="stylesheet">

    <!-- WIZARD ASSETS CUSTOMS-->
    <link href="{{ assets('assets/wizard-form/css/paper-bootstrap-wizard.css') }}" rel="stylesheet" />
    <link href="https://netdna.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.css" rel="stylesheet">
  <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    <link href="{{ assets('assets/wizard-form/css/themify-icons.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ assets('assets/duallist/bootstrap-duallistbox.css') }}">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />


<!--     <link href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
 -->     <!-- Fonts and Icons -->
</head>

<body>

    <!-- header start -->
    {{ view("includes/header", $data); }}

    <!-- sidebar -->
    {{ view("includes/sidebar", $data); }}

    <div class="content">
        {{  view($views,$data); }}
        <!-- {{  view($data->views); }} -->

        </div>
    </div>


    <!-- footer -->
    {{ view("includes/footer"); }}

    <script src="//code.jquery.com/jquery-1.12.4.js"></script>
    <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    <script src="https://jeesite.gitee.io/front/jquery-select2/4.0/index_files/jquery.min.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
    <script src="//cdn.datatables.net/buttons/1.4.2/js/buttons.flash.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script src="//cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>
    <script src="//cdn.datatables.net/buttons/1.4.2/js/buttons.print.min.js"></script>

    <!-- scripts -->
    <script src="{{ assets('assets/libs/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ assets('assets/js//jquery.slimscroll.min.js') }}"></script>
    <script src="{{ assets('assets/js/simcify.min.js') }}"></script>
    <!-- custom scripts -->
    <script src="{{ assets('assets/js/app.js') }}"></script>

    <!--  CUSTOMS for the Wizard -->
    <script src="{{ assets('assets/wizard-form/js/jquery.bootstrap.wizard.js') }}" type="text/javascript"></script>
    <script src="{{ assets('assets/wizard-form/js/paper-bootstrap-wizard.js') }}" type="text/javascript"></script>
    <script src="{{ assets('assets/wizard-form/js/jquery.validate.min.js') }}" type="text/javascript"></script>
    <script src="{{ assets('assets/duallist/jquery.bootstrap-duallistbox.js') }}"></script>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>


    <!--  END OF CUSTOMS for the Wizard -->

    <script>
        // In your Javascript (external .js resource or <script> tag)

    // jQuery( document ).ready(function( $ ) {
      // Code that uses jQuery's $ can follow here.

        // $('.js-example-basic-single').select2();
    // });
      var demo1 = $('select[name="approval_select[]"]').bootstrapDualListbox(
          {
              nonSelectedListLabel: 'Non-selected',
            selectedListLabel: 'Selected',
            preserveSelectionOnMove: 'moved',
          }
      );
    </script>
    <script type="text/javascript">

      $(document).ready(function() {
        $("#workflowType").change(function(){
          var type = $("#workflowType").val();
          alert(type);
        });
      });
      $(document).ready(function() {
            $('#signer-datatable').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5'
                ]
            });
        });

        $(document).ready(function() {
            $('#js-example-basic-multiple').select2();
        });
    </script>
</body>

</html>
