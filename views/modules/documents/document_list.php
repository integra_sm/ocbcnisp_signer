  <div class="row">
    <div class="col-md-12">
      <div class="light-card p-b-3em">
        <table id="signer-datatable" class="table display companies-list">
                        <thead>
                            <tr>
                                <th></th>
                                <th class="text-center w-70">Documents</th>
                                <th>Department</th>
                                <th>Note</th>
                                <th>Requestor</th>
                                <th class="text-center">Status</th>
                                <th class="text-center w-70">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if ( count($query) > 0 )
                            @foreach ( $query as $index => $request )
                            <tr>
                                <td class="text-center">{{ $index + 1 }}</td>
                                <td class="text-center">

                                </td>
                                <td>

                                </td>
                                <td class="text-center">

                                </td>

                                <td class="text-center">
                                    <div class="dropdown">
                                        <span class="company-action dropdown-toggle" data-toggle="dropdown"><i class="ion-ios-more"></i></span>
                                        <ul class="dropdown-menu" role="menu">
                                            <li role="presentation">
                                                <!-- @if ( $request['data']->status == "Pending" )
                                                <a class="request-remind" data-id="{{ $request['data']->id }}" href="">Remind</a>
                                                <a class="send-to-server-click"  data="requestid:{{ $request['data']->id }}|csrf-token:<?=csrf_token();?>" url="<?=url("Request@cancel");?>" warning-title="Are you sure?" warning-message="This request will be canceled." warning-button="Cancel Now" loader="true" href="">Cancel</a>
                                                @endif
                                                <a class="send-to-server-click"  data="requestid:{{ $request['data']->id }}|csrf-token:<?=csrf_token();?>" url="<?=url("Request@delete");?>" warning-title="Are you sure?" warning-message="This request will be deleted parmanently." warning-button="Delete Now" loader="true" href="">Delete</a> -->
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="6" class="text-center">It's empty here</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
      </div>
    </div>
  </div>
