
<style type="text/css" media="screen">
 .select2-container--default .select2-selection--multiple .select2-selection__rendered li {
    list-style: none;
    color: #000 !important;
}
</style>
        <div class="row">
            <div class="col-md-12">
                <div class="light-card p-b-3em">
                    <div class="wizard-container">
                        <div class="card wizard-card" data-color="red" id="wizardProfile">
                            <form action="{{ uri('documents/upload/file') }}" class="simcy-form" loader="true" method="POST" enctype="multipart/form-data">

                                <div class="wizard-header text-center">
                                    <h3 class="wizard-title">Uploads Documents</h3>
                                </div>

                                <div class="wizard-navigation">
                                    <div class="progress-with-circle">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="3" style="width: 33%;"></div>
                                    </div>
                                    <ul>
                                        <li>
                                            <a href="#uploads" data-toggle="tab">
                                                <div class="icon-circle">
                                                    <i class="ti-files"></i>
                                                </div>
                                                Uploads
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#signrequest" data-toggle="tab">
                                                <div class="icon-circle">
                                                    <i class="ti-pencil-alt"></i>
                                                </div>
                                                Signature Request
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#submission" data-toggle="tab">
                                                <div class="icon-circle">
                                                    <i class="ti-save-alt"></i>
                                                </div>
                                                Submission
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="tab-content">
                                    <div class="tab-pane" id="uploads">
                                        <div class="row">
                                        <p>Only PDF allowed.</p>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label>File name <i>(Description)</i></label>
                                                        <input type="text" class="form-control" name="name" autocomplete="off" placeholder="File name" data-parsley-required="true" >
                                                        <input type="hidden" name="folder" value="1">
                                                        <input type="hidden" name="csrf-token" value="<?=csrf_token();?>" />
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label>Category</label>
                                                        <select name="category" class="form-control">
                                                          @foreach($document_category as $cat)
                                                            <option value="{{ $cat->category_id }}">{{ $cat->category_name }}</option>
                                                          @endforeach
                                                        </select>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label>Choose file</label>
                                                        <input type="file" name="file" class="dropify" data-parsley-required="true" data-allowed-file-extensions="pdf" >
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="signrequest">
                                        <div class="row " style="margin-top:15px;"  >
                                            <div class="container">
                                                <div class="col-sm-12">
                                                    <div class="col-sm-3">
                                                        <label>Workflow Method :</label>
                                                    </div>
                                                    <div class="col-sm-4">
                                                       
                                                        <label><input type="radio" name="workflowType" value="SEQUENCE" checked> Sequence</label>
                                                        <label><input type="radio" name="workflowType" value="PARALEL" > Paralel </label>
                                                        <label><input type="radio" name="workflowType" value="COMBINE" >Combine</label>
                                                    </div>

                                                </div>


                                                <div class="col-sm-10">
                                                   <div class="panel panel-default row" style="margin-top:10px;">
                                                    <select id="js-example-basic-multiple" name="approval[]" multiple="multiple">
                                                  @foreach($employee as $row)
                                                             <option value="{{ $row->employee_id }}">{{ $row->employee_name }} // {{ $row->employee_email }}</option>

                                                    @endforeach
                                                </select>

                                                   </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="submission">
                                        <div class="row">

                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label>Messages to Recipients</label>
                                                    <textarea name="notes" autofocus="true" rows="5" columns="10" class="form-control"></textarea>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="wizard-footer">
                                    <div class="pull-right">
                                        <input type='button' class='btn btn-next btn-fill btn-danger btn-wd' name='next' value='Next' />
                                        <input type='submit' class='btn btn-finish btn-fill btn-danger btn-wd' name='finish' value='Finish' />
                                    </div>

                                    <div class="pull-left">
                                        <input type='button' class='btn btn-previous btn-default btn-wd' name='previous' value='Previous' />
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </form>
                        </div>
                    </div> <!-- wizard container -->
                    </div>
                </div>
            </div>

