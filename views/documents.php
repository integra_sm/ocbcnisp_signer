
 <div class="page-title documents-page" style="overflow:visible;">
    <div class="row">
        <div class="col-md-6 col-xs-6">
            <h3>Documents</h3>
            <p class="breadcrumbs text-muted"><span class="home-folder"></span></p>
        </div>
        <div class="col-md-6 col-xs-6 text-right page-actions">
            <button href="" class="btn btn-default go-back hidden-xs"><i class="ion-ios-arrow-back"></i> Back</button>
            <!--
            <div class="dropdown">
                <a href="{{ uri('uploads') }}" class="btn btn-primary dropdown-toggle" ><i class="ion-plus"></i> New Files </a>

            </div>
        -->
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
      <div class="light-card p-b-3em">
        <table id="signer-datatable" class="table display companies-list">
            <thead>
                <tr>
                    <th></th>
                    <th class="text-center w-70">Documents Name</th>
                    <th>Requestor</th>
                    <th>Department</th>

                    <th class="text-center">Status</th>
                    <th class="text-center">Request Date</th>

                    <th class="text-center w-70">Action</th>
                </tr>
            </thead>
            <tbody>
                @if ( count($query) > 0 )
                @foreach ( $query as $index => $request )
                <tr>
                    <td class="text-center">{{ $index + 1 }}</td>
                    <td class="text-center">
                        <a href="{{ uri('document/').$request->document_key }}" title="Go To Document">{{ $request->document_name }}</a>
                    </td>
                    <td>
                        {{ $request->requestor_email }}
                    </td>
                    <td>
                        {{ $request->name }}
                    </td>
                    <td>
                        {{ $request->status }}
                    </td>
                    <td>
                        {{ date("F j, Y H:i", strtotime($request->created_time)) }}
                    </td>

                    <td class="text-center">
                        <div class="dropdown">
                            <span class="company-action dropdown-toggle" data-toggle="dropdown"><i class="ion-ios-more"></i></span>
                            <ul class="dropdown-menu" role="menu">
                                <li role="presentation">
                                    <a class="fetch-display-click" data="customerid:{{ $customer['user']->id }}|csrf-token:<?=csrf_token();?>" url="<?=url("Customer@updateview");?>" holder=".update-holder" modal="#update" href="">Edit</a>
                                                <a class="send-to-server-click"  data="customerid:{{ $customer['user']->id }}|csrf-token:<?=csrf_token();?>" url="<?=url("Customer@delete");?>" warning-title="Are you sure?" warning-message="This customer's profile and data will be deleted." warning-button="Continue" loader="true" href="">Delete</a>
                                </li>
                            </ul>
                        </div>
                    </td>
                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="6" class="text-center">It's empty here</td>
                </tr>
                @endif
            </tbody>
        </table>
      </div>
    </div>
</div>

<!-- @if ( count($query) > 0 )
    <script>
        $(document).ready(function() {
            $('#signer-datatable').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5'
                ]
            });
        });
    </script>
    @endif -->
