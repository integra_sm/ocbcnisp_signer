<?php
namespace Simcify\Controllers;

use Google_Client;
use Google_Service_Drive;
use Simcify\Str;
use Simcify\File;
use Simcify\Mail;
use Simcify\Auth;
use Simcify\Signer;
use Simcify\Database;

class ModulesDocuments{

  public function lists(){
    $data =array();
    $user = Auth::user();
    $data['query'] = Database::table("signer_request")->get();
    $data['views']  = "documents/document_list";
    return view('modules/layouts',$data);
  }
}

?>
