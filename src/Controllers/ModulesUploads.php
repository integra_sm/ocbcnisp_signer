<?php

namespace Simcify\Controllers;

use Google_Client;
use Google_Service_Drive;
use Simcify\Str;
use Simcify\File;
use Simcify\Mail;
use Simcify\Auth;
use Simcify\Signer;
use Simcify\Database;


ini_set('display_errors', 'On');
error_reporting(E_ALL);

class ModulesUploads{



    public function new_wizard(){
        $data =array();
        $data['user'] = Auth::user();
        $data['employee'] = Database::table("employee")->get();
        $data['document_category'] = Database::table("document_category")->get();
        // exit(print_r($data['document_category']));
        $data['views']  = "modules/documents/upload_wizard";
        return view('modules/layouts',$data);

    }

    // public function save_files(){
    //     header('Content-type: application/json');

    //     $user = Auth::user();
    //   $input = input()->post;

    //   $data = array(
    //     "company" => 1,
    //     "requestor_id" => 11783,
    //     "requestor_email" => $user->email,
    //     "department_id" => 1,
    //     "requestor_note" => input('notes'),
    //     "status" => "OPEN",
    //     "workflow_method" => input('workflowType'),
    //     "next_order" => 1,

    // );
    //     $insert_one = Database::table("signer_request")->insert($data);
    //   if($insert_one){
    //         $doUpload = array(
    //         "file" => $_FILES['file'],
    //         "request_id" => Database::table("signer_request")->insertId(),
    //         "created_by" => $user->id,
    //         "folder" => 1,
    //         "document_name" => "",
    //         "category" => input('category'),
    //         "status" => "Unsigned",
    //         "edited" => "No",
    //         "accesbility" => "Everyone",
    //         "public_permission" => "sign_edit",
    //         "source" => "form",
    //          "document_key" => Str::random(32),
    //          "activity" => 'File uploaded by <span class="text-primary">'.escape($user->fname.' '.$user->lname).'</span>.'

    //       );

    //       // exit(json_encode($data));
    //       $upload = $this->doUploads($doUploads);
    //       if($upload){
    //         // exit(json_encode("successs"));
    //         // return responder("success", "Upload Complete", "File successfully uploaded. ");
    //         exit(json_encode(responder("success", "Data Has Been Uploaded", "Success Uploaded","redirect('".env("APP_ADDRESS")."/documents');", true,'swal')));
    //         // exit(json_encode(responder("success", "Saved", "Data Has Been Uploaded","", false)));
    //       }else{
    //         exit(json_encode(responder("error", "Oops!", "Delete failed, please try again.","", true, "toastr")));
    //         }
    //   }else{

    //      exit(json_encode(responder("error", "Oops!", "Incorrect File Uploads.")));
    //   }





    //     // exit(json_encode(responder("success", "", "","documentsCallback()", false)));
    //   // return json_encode($input);

    // }

    // public function doUploads($data){
    //     $upload = File::upload(
    //             $data['file'],
    //             "files",
    //             array(
    //                 "source" => $data['source'],
    //                 "allowedExtensions" => "pdf"
    //             )
    //         );
    //     if ($upload['status'] == "success") {
    //         $data['request_id'] = $data['request_id'];
    //         $data['folder'] = $data['folder'];
    //         $data['document_name'] = $data['name'];
    //         $data['category'] = $data['category'];
    //         $data["filename"] = $upload['info']['name'];
    //         $data["size"] = $upload['info']['size'];
    //         $data['status'] = $data['status'];
    //         $data['edited'] = $data['edited'];
    //         $data['accessbility'] = $data['accessbility'];
    //         $data['public_permission'] = $data['public_permission'];
    //         $data["extension"] = $upload['info']['extension'];
    //         $activity = $data['activity'];
    //         unset($data['file'], $data['source'], $data['activity'],$data['extension']);
    //        $doSaveFiles = Database::table("signer_files")->insert($data);
    //        if($doSaveFiles){

    //             return true;
    //         }else{
    //             return false;
    //         }

    //     }else{
    //         return $upload['message'];
    //     }


    // }
}
